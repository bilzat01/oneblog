<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 杨维杰 <917647288.qq.com>
// +----------------------------------------------------------------------

namespace Home\Widget;
use Think\Action;

/**
 * 分类widget
 * 用于动态调用分类信息
 */

class CommonWidget extends Action{

	public function _initialize(){
		$this->cache = get_front_cache();
	}

	/**
	 * 获取单页的列表
	 */
	public function single(){
		$this->assign('single', $this->cache['single_list']);
		$this->display('Widget/single');
	}

	/**
	 * 最新文章
	 */
	public function new_article(){
		$this->assign('new_article', $this->cache['new_article']);
		$this->display('Widget/new_article');
	}

	/* 显示指定分类的同级分类或子分类列表 */
	public function cates(){
        $this->assign('category', $this->cache['cate']);
		$this->display('Widget/lists');
	}

	/**
	 * 归档显示
	 */
	public function archive(){
		$this->assign('archive', $this->cache['date']);
		$this->display('Widget/archive');
	}

	public function tags(){
		$tags = M('Tags')->getField('title',true);
		$this->assign('tags',$tags);
		$this->display('Widget/tags');
	}

}
