观察者模式
===========

> 观察者模式在现实的应用系统中有许多重要应用：比如像当当网、京东商城一类的电子商务网站，
如果收藏某件商品，那么当该商品降价时，系统给您发送手机短信或邮件；生日到了，系统发生祝福邮件。

观察者模式中，有三个基本角色：观察者，主体，事件。

流程
-------

事件发生后，调用主体的方法来设置事件的状态然后主体通知所有观察者来查询事件状态。观察者得到状态，执行相应的动作。

事件
-------

事件的发生是与主体和观察者无关的，它只需要在发生后，去触发主体的某个方法即可。主体可以处理多种事件。

主体
----

主体的作用是：添加/移除主体持有的观察者，通知所有观察者事件，还要能设置和读取事件状态。

因此主体必须有一个添加观察者的 **`attach()`**方法，移除观察者的 **`detach()`**方法，
通知观察者的 **`notify()`**方法，获取状态的 **`getStatus()`**方法，
设置状态的 **`setStatus()`**方法,供事件发生时调用的 **`handlerXXX()`**方法。

必须有一个成员保存观察者，一个成员保存保持各种事件状态


观察者
--------

观察者得到主体的通知后，要从主体中读取最新的事件，然后根据事件执行操作。

观察者的构造函数必须完成两个基本功能：参数接受主体对象，保存主体对象到属性中，并调用主体对象的 `attach` 方法将自己添加到主体的观察者列表。

当事件调用主体对象的 `notify()`时，会遍历所有观察者，执行他们的`update()`方法，观察者将主体传过来的对象与自己所保存的主体对象进行严格比较($this->login===$observable)，如果为真，说明传来的参数正是自己所观察的主体。于是将这个主体对象转交 doupdate()方法进行具体处理，doupdate()调用主体中的状态，根据状态执行相应的动作。

```
<?php
class Observable implements SplSubject
{
    private $storage;

    function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    function attach(SplObserver $observer)
    {
        $this->storage->attach($observer);
    }

    function detach(SplObserver $observer)
    {
        $this->storage->detach($observer);
    }

    function notify()
    {
        foreach ($this->storage as $obj) {
            $obj->update($this);
        }
    }
    //...
}

abstract class Observer implements SplObserver
{
    private $observable;

    function __construct(Observable $observable)
    {
        $this->observable = $observable;
        $observable->attach($this);
    }

    function update(SplSubject $subject)
    {
        if ($subject === $this->observable) {
            $this->doUpdate($subject);
        }
    }

    abstract function doUpdate(Observable $observable);
}

class ConcreteObserver extends Observer
{
    function doUpdate(Observable $observable)
    {
        //...
    }
}

$observable = new Observable();
new ConcreteObserver($observable);
```

![观察者模式](images/guanchazhe.png)
