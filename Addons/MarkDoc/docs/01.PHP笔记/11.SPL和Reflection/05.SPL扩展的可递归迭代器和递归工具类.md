####SPL扩展中与递归有关的Class分两种：

1. 可递归的迭代器：所有实现了RecursiveIterator接口的对象，都可以被递归工具递归迭代。但自身不能递归迭代。
1. 递归工具类：RecursiveIteratorIterator类，对可递归迭代对象进行递归迭代。它只是个递归工具，实现自OuterIterator，所以没有其他多余的功能。

	必要情况下，你可以把它的实例当作一个普通的迭代对象与其他迭代对象封装成一个复合的迭代对象使用。

RecursiveIterator接口的实现
=========================

RecursiveArrayIterators
------------------------

> 可递归数组迭代类,该类继承自ArrayIterator


```php
RecursiveArrayIterator extends ArrayIterator implements RecursiveIterator ArrayAccess , SeekableIterator , Countable , Serializable{
/* 方法 */
public __construct ( mixed $array )
public RecursiveArrayIterator getChildren ( void )
public bool hasChildren ( void )
}
```

RecursiveDirectoryIterator
---------------------

> 可递归目录迭代类

!!!!!注意：默认设置下该迭代器含有 . 和 .. 目录元素

建议在构造方法传入flag参数：FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_SELF | FilesystemIterator::SKIP_DOTS ]

!!!!!如果使用了CURRENT_AS_PATHNAME，那么getChildren()得到的不是迭代器对象而是字符串，就不具备递归条件了。

!!!!!hasChildren()对空目录也会返回true


```php
RecursiveDirectoryIterator extends FilesystemIterator implements Traversable , Iterator , SeekableIterator , RecursiveIterator {
/* 方法 */
public __construct ( string $path [, int $flags = FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO ] )
public bool   hasChildren ([bool $allow_links])
public mixed  getChildren ( void )    //根据当前元素的绝对路径，实例化出一个RecursiveDirectoryIterator对象返回
public string getSubPath ( void )     //返回文件所属的目录名的相对路径
public string getSubPathname ( void ) //返回文件相对于构造参数目录的相对目录
public string key ( void )
public void   next ( void )
public void   rewind ( void )
}
```

可递归对象的过滤类
----------------

> 这些类可以创建一个可递归对象，它内部保存着一个可递归对象(构造方法传入)，迭代是实际发生在内部保存的这个对象上面，但在迭代到一个元素时，会用自己的accept()方法先检查是否符合一定的条件，如果不符合则跳过该元素。

> SPL提供了3个能过滤可递归对象的迭代器，这3个类实现了RecursiveIterator接口，以供RecursiveIteratorIterator递归迭代工具使用。
> 他们的{r:构造方法只能接收RecursiveIterator的实例}，即只接受可递归迭代对象作为参数。

> 要对这些可对象进行递归迭代，可以使用自定义函数进行递归调用，更方便的是使用 递归迭代工具类

###RecursiveFilterIterator

> 可递归对象的过滤抽象类

> 在子类中必须实现accept()抽象方法

该类继承自FilterIterator抽象类，只是重写了构造方法使其只接收可递归对象(RecursiveIterator实例)作为参数，并增加了hasChildren()和getChildren()方法，这两个方法会被递归迭代工具在递归时调用。

如果你要创建自定义的递归函数进行递归迭代，那么就必须用到这两个方法进行元素判断和获取可递归元素。

```php
RecursiveFilterIterator extends FilterIterator implements Iterator , Traversable , OuterIterator , RecursiveIterator {
/* 方法 */
public __construct ( RecursiveIterator $iterator )
abstract bool FilterIterator::accept ( void )
public RecursiveIterator getChildren ( void )
public bool  hasChildren ( void )
```

### ParentIterator


> 这个递归过滤器会检查迭代到的元素，如果该元素是一个可以有子元素的元素(“可以有”的意思是说：空目录，空数组，空的SimpleXMLIterator元素对象也会返回true)，则接受该元素，否则跳过该元素。


ParentIterator extends RecursiveFilterIterator implements RecursiveIterator , OuterIterator , Traversable , Iterator {
/* 方法 */
public __construct ( RecursiveIterator $iterator )
public bool accept ( void )
public ParentIterator getChildren ( void )
public bool hasChildren ( void )
public void next ( void )
public void rewind ( void )
}

###RecursiveCallbackFilterIterator

> 用回调函数来决定是否迭代该元素。

> {y:callback函数需传入三个参数 ($current, $key, $iterator)}


```php
RecursiveCallbackFilterIterator extends CallbackFilterIterator implements OuterIterator , Traversable , Iterator , RecursiveIterator {
/* 方法 */
public __construct ( RecursiveIterator $iterator , string $callback )
public RecursiveCallbackFilterIterator getChildren ( void )
public void hasChildren ( void )
/* 继承的方法 */
public string CallbackFilterIterator::accept ( void )
}
```

###RecursiveRegexIterator


> 用正则表达式来决定是否迭代该元素。显然，current()必须能够返回字符串或数组才可以用这个类。


```php
RecursiveRegexIterator extends RegexIterator implements RecursiveIterator {
/* 方法 */
public __construct ( RecursiveIterator $iterator , string $regex [, int $mode = self::MATCH [, int $flags = 0 [, int $preg_flags = 0 ]]] )
public RecursiveRegexIterator getChildren ( void )
public bool hasChildren ( void )
}
```

递归迭代工具类
============


RecursiveIteratorIterator
---------------------------

key()和current()返回值是什么是当前正在迭代的内部迭代器对象类型决定的。{g:RecursiveIteratorIterator中的key()和current()仅仅是在内部调用了当前迭代到的对象的同名方法并将其返回值返回。}


> 将RecursiveIterator的实例作为参数传入该类的构造方法，即可得到一个递归迭代工具对象，直接foreach便可以递归遍历。

> 可以用它来处理RecursiveArrayIterator对象，SimpleXMLIterator对象，ParentIterator对象，RecursiveRegexIterator对象，RecursiveCallbackFilterIterator对象，RecursiveTreeIterator对象

{y:需要记住的主要方法包括：构造方法，setMaxDepth，getMaxDepth,getDepth}

#### 以下方法是事件方法，都是空方法，扩展时可以覆写实现：

* beginIteration ：在开始递归遍历之前先执行该方法（用于初始化）
* endIteration ：当递归遍历全部结束后（RecursiveIteratorIterator::valid()返回FALSE后）执行该方法（例如执行GC）。
* beginChildren：在遍历本层第一个子元素之前先执行该方法
* endChildren：当结束本层递归时执行该方法。
* nextElement：在迭代每一个元素之前执行该方法。

手动递归迭代
--------------

直接在循环语句中，使用valid()和next()就可以完成最基本的递归迭代。是否可递归的判断和执行递归是自动处理的。


要返回当前迭代到的元素对象，使用callGetChildren()方法


```php
RecursiveIteratorIterator implements OuterIterator , Traversable , Iterator {
/* MODE常量 */
const integer LEAVES_ONLY = 0 ;      //mode:返回的元素都是不能迭代的基本叶元素，可迭代枝元素自身不会被返回。
const integer SELF_FIRST = 1 ;       //mode:遇到可迭代元素，首先返回元素自身，下一次开始迭代其中的元素
const integer CHILD_FIRST = 2 ;      //mode:遇到可迭代元素，首先迭代，迭代完成后再返回自身
/*flag常量*/
const integer CATCH_GET_CHILD = 16 ; //flags: 忽略在调用getChildren()时可能抛出的异常
/* 方法 */
       
public __construct ( Traversable $iterator [,int $mode=RecursiveIteratorIterator::LEAVES_ONLY [,int $flags=0]])
/*空方法*/
public void  beginChildren ( void )  
public void  endChildren ( void )                 
public void  endIteration ( void )                 
public void  beginIteration ( void )
public void  nextElement ( void ) 
       
/*递归深度*/
public int   getDepth ( void )                         //返回当前递归的深度.
public mixed getMaxDepth ( void )                      //返回允许的最大递归深度.
public void  setMaxDepth ([string $max_depth = -1] )   //设置允许的最大递归深度
       
//手动迭代相关方法
public mixed current ( void )                      //
public mixed key ( void )
public void  next ( void )
public void  rewind ( void )
public bool  valid ( void )
       
//下面的方法只是用于获取对象，它们不会影响到递归迭代过程
public bool callHasChildren ( void )               //判断当前迭代到的元素是否有子元素.
public RecursiveIterator callGetChildren ( void )  //返回当前迭代到的元素对象
public RecursiveIterator getSubIterator ( void )  
public iterator getInnerIterator ( void )         
}
```


```php
<?php
$array = array(
    array(
        array(
            array(
                'leaf-0-0-0-0',
                'leaf-0-0-0-1'
            ),
            'leaf-0-0-0'
        ),
        array(
            array(
                'leaf-0-1-0-0',
                'leaf-0-1-0-1'
            ),
            'leaf-0-1-0'
        ),
        'leaf-0-0'
    )
);
       
$iterator = new RecursiveIteratorIterator(
    new RecursiveArrayIterator($array),
    0
);
foreach ($iterator as $key => $leaf) {
    echo "$key => $leaf", PHP_EOL;
}
?>
```


```php
<?php
$mdir = new RecursiveDirectoryIterator('/home/zhuyajie/Dropbox/study/XMLlesson');
$rii=new RecursiveIteratorIterator ($mdir,RecursiveIteratorIterator::SELF_FIRST,RecursiveIteratorIterator::CATCH_GET_CHILD);
$i=0;//设置一个递归统计变量，可以统计到递归的总元素，可以限制递归的总元素，也可以避免代码错误导致的死循环。
$rii->rewind(); //不可省略此句。
while ( $rii->valid() && $i<500 ) {
    echo $rii->key();
    echo "\n";
    $rii->next();
    $i++;
}
```


```php
<?php
/*创建一个可递归目录迭代器*/
$mdir = new RecursiveDirectoryIterator('/home/zhuyajie/Dropbox/study/');
/*创建一个递归工具对象*/
$rii=new RecursiveIteratorIterator ($mdir,RecursiveIteratorIterator::SELF_FIRST,RecursiveIteratorIterator::CATCH_GET_CHILD);
/*创建一个可递归数组迭代器*/
$arr=array(1,2,array('a','b'));
$rdi = new RecursiveArrayIterator($arr);
$arri=new RecursiveIteratorIterator($rdi);
/*组合成一个迭代对象*/
$compac=new AppendIterator();
$compac->append($arri);
$compac->append($rii);
/*遍历组合后的迭代对象*/
foreach ( $compac as $k=> $v ) {
    echo $k.'==='.$v;
    echo "\n";
}
```
