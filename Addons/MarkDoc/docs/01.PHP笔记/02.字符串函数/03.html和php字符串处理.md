# 实体表

html语法实体符号

|显示结果|描述|实体名称	|实体编号|
|---|-------|--------|--------|
| 	|空格	|`&nbsp;`|`&#160;`|
|<	|小于号	|`&lt;`	 |`&#60;`  |
|>	|大于号	|`&gt;`	 |`&#62;`  |
|&	|和号	|`&amp;` |`&#38;` |
|"	|引号	|`&quot;`|`&#34;` |
|'	|撇号 	|`&apos;`|(IE不支持)`&#39;`|

其他html常用的字符实体

|显示结果|描述|实体名称|实体编号|
|---|---|------|----------|
|¢	|分	 |`&cent;` |`&#162;`|
|£	|镑	 |`&pound;`|`&#163;`|
|¥	|日圆|`&yen;`  |`&#165;`|
|§	|节	 |`&sect;` |`&#167;`|
|©	|版权|`&copy;` |`&#169;`|
|®	|注册商标|`&reg;`|`&#174;`|
|×	|乘号|`&times;`  |`&#215;`|
|÷	|除号|`&divide;` |`&#247;`|

# 相关函数

<p class="api">
string htmlentities (string $string[,int $flags = ENT_COMPAT|ENT_HTML401[,string $encoding = 'ISO-8859-1'[,bool $double_encode = true]]])
</p>

> 将字符串中html实体字符转换成实体符号或实体编码,例如：`" < > & © £`，一定要设置正确的字符集，否则不识别的字符都会被转换。

$string：待转换字符串

$flags：附加选项（可以使用 | 来同时设置多个选项）

|常量          |描述                               |
|--------------|-----------------------------------|
|ENT_COMPAT    |默认：不转换单引号，只转换双引号   |
|ENT_QUOTES    |单双引号都进行转换                 |
|ENT_NOQUOTES  |单双引号都不转换                   |
|ENT_IGNORE    |5.3添加：单双引号都不转换，删除不能识别的字符，而不是用空字符串代替。不建议使用该项，可能有安全隐患|
|ENT_SUBSTITUTE|  PHP5.4添加：Replace invalid code unit sequences with a Unicode Replacement Character U+FFFD (UTF-8) or &#FFFD; (otherwise) instead of returning an empty string.|
|ENT_DISALLOWED|  PHP5.4添加：Replace invalid code points for the given document type with a Unicode Replacement Character U+FFFD (UTF-8) or &#FFFD; (otherwise) instead of leaving them as is. This may be useful, for instance, to ensure the well-formedness of XML documents with embedded external content.|
|ENT_HTML401   |  Handle code as HTML 4.01. PHP5.4添加|
|ENT_XML1      |  Handle code as XML 1. PHP5.4添加|
|ENT_XHTML     |  Handle code as XHTML. PHP5.4添加|
|ENT_HTML5     |  Handle code as HTML 5 PHP5.4添加|

$encoding 字符集，默认为ISO-8859-1，所以{r:通常需要手段设定该项，否则转换后的多字节字符出现乱码}；PHP5.4以后默认为UTF-8；支持GB2312和BIG5以及日俄字符集。

<p class="api">
string html_entity_decode ( string $string[,int $flags = ENT_COMPAT | ENT_HTML401 [,string $encoding = 'UTF-8']])
</p>

<p class="api">
string htmlspecialchars ( string $string [,int $flags = ENT_COMPAT | ENT_HTML401 [,string $encoding = 'ISO-8859-1' [,bool $double_encode = true]]] )
</p>

功能及参数同htmlentities();
但这个函数只能转换`<,>,&,和单双引号(默认也是只转换双引号)`，其他所有符号都不会转换。

<p class="api">
string htmlspecialchars_decode ( string $string [, int $flags = ENT_COMPAT | ENT_HTML401 ] )
</p>

<p class="api">
string nl2br ( string $string [, bool $is_xhtml = true ] )
</p>

把字符串中的\n替换为<br />

<p class="api">
string strip_tags ( string $str [, string $allowable_tags ] )
</p>

去除字符串中的html和php标签，HTML 注释和 PHP 标签。

$allowable_tags设置需要保留的HTML标签，例如：

```php
// 允许 <p> 和 <a>
echo strip_tags($text, '<p><a>');
```
!!!!!PHP 标签是硬编码处理的，所以无法被排除。

<p class="api">
string fgetss ( resource $handle [, int $length [, string $allowable_tags ]] )
</p>

从文件中读取一行并去除html和php标签。HTML 注释和 PHP 标签是硬编码处理的，所以无法被排除。

$allowable_tags设置需要保留的HTML标签

$length 读取时的最大长度

<p class="api">
string php_strip_whitespace ( string $filename )
</p>

将一个php文件的多余空白和注释删除后返回

<p class="api">
mixed highlight_string ( string $str [, bool $return = false ] )
</p>

使用PHP内置的语法高亮器所定义的颜色，打印输出或者返回输出或者返回PHP代码语法高亮的html代码。 
