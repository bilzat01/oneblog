自定义session处理机制
-------------------

使用自定义函数来重载内建的session_start,session_close,session_destroy函数和session的读、写、GC操作。

!!!!!必须先把session机制设为自定义方式`ini_set("session.save_handler", "user")`

<p class="api">bool session_set_save_handler (callable $open ,callable $close,callable $read,callable $write,callable $destroy,callable $gc)</p>

!!!!!重载的回调函数，除了read()函数必须返回从文件或数据库查询到的session数据的序列化字符串，其他函数都return一个布尔值表示这个逻辑是否执行成功。

####回调函数要求：

<p class="api">bool open(string $savePath, string $sessionName)</p>
* 重载session_start()
* 可以写一些安全验证的代码，如果访问不合法重生成新的sessionid或者返回false来终止session

<p class="api">bool close()</p>

* 重载session_write_close
* 脚本执行结束和write方法执行以后会自动执行该方法

<p class="api">string read(string $sessionId)</p>

* 在该方法要实现session序列化字符串数据的读取（从文件或数据库或内存）
* 无需进行反序列化操作，php会自动对返回的字符串执行session_decode恢复到$_SESSION。

<p class="api">write(string $sessionId, string $data)</p>

* 每当session变量的值改变或者创建了新变量或者脚本执行结束，write回调方法会被调用，首先php会自动将$_SESSION变量执行session_encode序列化以后作为$data参数发送给write
* 不管有没有新内容需要写入，每次访问都执行一次，使session文件修改时间改变，session过期时间更新。
* 在该方法要实现把session数据序列化以后的保存(写入文件或数据库或内存)
* 该方法内部的语句的debugging信息无法被输出到浏览器

<p class="api">bool destroy($sessionId)</p>

* 重载session_destroy() 
* 该方法要实现session数据的删除（删除文件或数据库的记录或内存中的记录），清除客户端的sessionname的cookie值，返回布尔值表示成功或失败
* 该方法会在session_destroy() 和session_regenerate_id()被调用时执行

<p class="api">bool gc($lifetime)</p>

* gc机制重载
* 每次执行gc时该方法被调用
* 该方法要实现垃圾session的判定、寻找和删除


面向对象的重载方式：
------------------- 

<p class="api">bool session_set_save_handler ( SessionHandlerInterface $sessionhandler [, bool $register_shutdown = true ] )］</p>

参数$sessionhandler接受一个 SessionHandlerInterface接口的实例。

```php
SessionHandlerInterface {
    /* 方法 */
    public bool close ( void )
    public bool destroy ( string $session_id )
    public bool gc ( string $maxlifetime )
    public bool open ( string $save_path , string $session_id )
    public string read ( string $session_id )
    public bool write ( string $session_id , string $session_data )
}
```


####session的mysql持久化实例

```php
/**
 * 后盾网  http://www.houdunwang.com
 * 2011-6-21 上午11:03:48
 */
/**
 * Create Table: CREATE TABLE `session` (
  `sid` char(32) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `mtime` int(10) DEFAULT NULL,
  `ip` char(15) DEFAULT NULL,
  `card` char(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=gbk
 */
class session{
    //数据库操作句柄
    protected static $db;
    //SESSION表
    static $table='session';
    //SESSION过期时间
    static $max_time=600;
    //客户端身份信息
    protected static $card;
    //SESSION初始化
    static function run(){
        if(ini_get("session.save_handler") == "user" || ini_set("session.save_handler", "user")){
            session_set_save_handler(
                array(__CLASS__,"start"),
                array(__CLASS__,"close"),
                array(__CLASS__,"read"),
                array(__CLASS__,"write"),
                array(__CLASS__,"destroy"),
                array(__CLASS__,"gc")
            );
            self::$card = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
            self::$db   = new mysqli("localhost","root","","edu");
            !mysqli_connect_errno() or die("数据库连接错误");
            self::$db->query("SET NAMES GBK");
            session_start();              
        }
    }
    static function start($path,$session_name){
        return true;
    }
    static function close(){
        self::gc(self::$max_time);
        self::$db->close();
        return true;
    }
    static function read($sid){
        $sql    = "SELECT `data` FROM ".self::$table." WHERE `sid` ='{$sid}' AND `card`='".self::$card."'";
        $result = self::$db->query($sql);
        $row    = $result->fetch_assoc();
        return self::$db->affected_rows>0?$row['data']:'';
    }
    static function write($sid,$data){
        $sql  = "SELECT `sid` FROM ".self::$table." WHERE `sid` ='{$sid}' AND `card`='".self::$card."'";   
        $result = self::$db->query($sql);
        $mtime = time();
        if(self::$db->affected_rows>0){
            $sql = "UPDATE ".self::$table." SET `data` ='{$data}',`mtime` ='{$mtime}'WHERE `sid`='{$sid}'";   
        }else{
            $sql = "INSERT INTO ".self::$table." (`sid`,`data`,`mtime`,`ip`,`card`) VALUES('{$sid}','{$data}','".time()."','{$_SERVER['REMOTE_ADDR']}','".self::$card."')";
        }
        return self::$db->query($sql)?true:false;
    }
    static function destroy($sid){
        $sql = "DELETE FROM ".self::$table." WHERE `sid`='{$sid}'";
        self::$db->query($sql);
        return true;
    }
    static function gc($max_time){
        $max_time = self::$max_time;
        $sql = "DELETE FROM ".self::$table." WHERE `mtime`<'".(time()-$max_time)."'";
        self::$db->query($sql);
        return true;
    }
}
session::run();
```