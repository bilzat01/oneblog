<p class="api">bool copy ( string $source , string $dest )</p>

相当于linux的 `cp -f   source   dest`

同样，它可以实现复制的时候改名

!!!!!注意：如果复制时目标文件名存在，会被覆盖。{y:只能复制文件，不能复制目录。}

{r:Note: Windows 兼容：如果复制一个零字节的文件，copy() 将返回 FALSE ，但文件也会被正确复制}

<p class="api">bool rename (string $oldname,string $newname[,resource $context ]) </p>

目录/文件的重命名或移动

相当于linux的 `mv -f  oldname  newname`

!!!!!注意：如果复制时目标文件名存在，会被覆盖。

<p class="api">bool unlink ( string $filename )</p>

删除指定的一个文件

相当于linux的 `rm -f filename`

<p class="api">string getcwd ( void )</p>

获得当前操作的目录

相当于linux的`pwd`命令

<p class="api">bool chdir ( string $directory )</p>

更改当前操作的目录

相当与linux的 `cd  directory` 

<p class="api">bool mkdir(string $pathname[,int $mode=0777[,bool $recursive=false[,resource $context]]])</p>

新建一个目录或递归创建多层目录，$recursive参数为true可递归方式建立多级目录

相当于linux的 `mkdir [-m] [-p] path`

默认的 mode 是 0777，mode 在 Windows 下被忽略


<p class="api">bool rmdir ( string $dirname )</p>

相当与linux的`rmdir`命令，{r:只能删除不在被使用中的空目录}


<p class="api">array scandir (string $directory [,int $sorting_order [,resource $context ]]) </p> 
 
相当于linux的 `ls -a  directory`

返回目录内所有文件名和目录名，返回一个数组

目录参照支持file://方式

注意：返回的数组中，有两个特殊元素`.`和`..`

<p class="api">array glob (string $pattern [, int $flags ])</p>

相当于相当于linux的 `find pattern`

返回一个包含有匹配文件／目录的数组。如果出错返回 FALSE。

#####!!有效标记为：

GLOB_MARK
:    在每个目录后面加一个目录分隔符

GLOB_NOSORT
:    按照文件在目录中出现的原始顺序返回（不排序）

GLOB_NOCHECK
:   如果没有文件匹配则返回用于搜索的模式

GLOB_NOESCAPE
:    不转义元字符

GLOB_BRACE
:   多重条件匹配，`glob('phpMyAdmin/{*.ico,*.txt}',GLOB_BRACE)`

GLOB_ONLYDIR
:   仅返回目录

GLOB_ERR
:   停止并读取错误信息（比如说不可读的目录），默认的情况下忽略所有错误

#####!!!!!小问题：glob()与scandir()的区别?

glob返回的结果中，不含有`.`和`..`，同时$pattern是一个目录匹配参数，支持相对目录和绝对目录，可以用通配符，但不能用file://协议格式 
