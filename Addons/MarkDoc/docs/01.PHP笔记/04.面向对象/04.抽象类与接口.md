抽象方法
=============

抽象方法是**抽象类**和**接口**中的方法：抽象类中使用**abstract**关键字定义，接口中的方法全部是抽象方法，无需关键字。

抽象方法没有方法体，即抽象方法不实现任何具体的内容，只提供一个抽象概念

抽象方法的语法规则： 

1. 子类和父类不能含有同名的抽象方法 
2. 子类重写抽象方法时，参数的顺序与参数约束必须完全一致。 
3. 如果子类是抽象类，则抽象方法就不必重写，{r:如果子类是普通类，则抽象方法必须重写。}
 
抽象类
============ 

> 类名使用**abstract**关键字修饰的类，称为抽象类，不强制抽象类一定要有抽象方法。
>
> 抽象类不能创建实例。

含有抽象方法的类必须用abstract定义为抽象类，以声明不能对它实例化，否则报致命错误。

抽象类可以继承抽象类，但抽象类不能extends接口，只能implements接口。 


语法：
 
```php
abstract class ABC{
    //常量和属性

    //抽象方法不用写具体代码，分号结尾
    abstract function fangfa();
    
    //其他方法
}
```

接口
===============

> 接口是一种纯粹的抽象，一个接口应专注于一种需求抽象，不与其他需求交叉耦合。通过实行接口来实现基于同一基本需求的多样化。 


* 定义：`interface Classname` 
* 接口名和子类的类名不能重复（因为接口也是类）
* 接口可以extends多个接口，用“,”号隔开 
* 类可以implentments多个接口，用“,”号隔开
* 接口内只有抽象方法和常量两种内容 
* 接口的常量不允许重写（而普通类和抽象类中的常量是允许重写的） 


```php
//接口定义
interface Father { 
    // 抽象方法
    function method();
    //其他抽象方法...
}
```

```php
///实现接口
class child implements father1,father2 {
      function method() {
      }
}

// 一个类可以同时继承一个父类和实现多个接口
class Sdudent extends Person implements Asian,American{
}
```


```php
interface a
{
    public function foo();
}
//接口继承接口   
interface b extends a
{
    public function baz(Baz $baz);
}
```

![接口的继承和实现](images/2.png)