
<p class="api">int preg_match(string $pattern,string $subject[,array &$matches[,int $flags=0[,int $offset=0]]])</p>

从subject中搜索一个正则表达式pattern的匹配.

$matches
: 将被填充为搜索结果。
: $matches[0]将包含pattern匹配到的完整文本, $matches[1] 将包含pattern表达式里面的第一个子匹配文本, 以此类推.

$flags
: 如果设为 **PREG_OFFSET_CAPTURE**，$matches得到的结果将是二维数组，每个匹配的元素都返回一个数组，内容是匹配到的文本和文本在$subject中的位置。

$offset
: 设定开始搜索的起始位置

!!!!!preg_match()返回值将是0次(不匹配)或1次, 因为 preg_match()在第一次匹配pattern后将会停止搜索


<p class="api">int preg_match_all(string $pattern,string $subject[,array &$matches[,int $flags=PREG_PATTERN_ORDER[,int $offset=0]]])</p>

搜索subject中所有与pattern相匹配的内容，并且将它们以flag指定顺序保存到matches中.

$flags设置$matches的保存方式，支持的参数：
PREG_PATTERN_ORDER
: 默认方式，结果为二维数组，$matches[0][0]为第一次匹配的完整匹配，$matches[0][1]为第2次匹配的完整匹配…；$matches[1][0]为第一次匹配的第一个子捕获，$matches[1][1]为第一次匹配的第2个子捕获

PREG_SET_ORDER
: 结果为二维数组，$matches[0] 包含第一次匹配得到的所有匹配($matches[0][0]为第一次匹配的完整匹配，其余为捕获的子组), $matches[1]以此类推.

PREG_OFFSET_CAPTURE
: 与preg_match相同，但结果为三维数组，因为每一次匹配都要保存。


返回完整匹配次数(可能是0), 或者如果发生错误返回FALSE.


<p class="api">mixed preg_replace(mixed $pattern,mixed $replacement,mixed $subject[,int $limit=-1[,int &$count]])</p>

搜索subject中匹配pattern的部分, 以replacement进行替换.  内部是通过preg_match_all工作，每一个完整匹配都会被替换。

##### pattern

正则表达式，可以使一个{g:字符串}或含有多个正则表达式的{g:数组}.

##### replacement

用于替换的{g:字符串}或字符串{g:数组}

如果这个参数是一个字符串, 而pattern 是一个数组, 那么每一个正则表达式的匹配都使用这一个字符串进行替换.

如果pattern和replacement 都是数组, 那么按索引一一对应；

如果replacement中的元素比pattern中的少, 多出来的pattern使用空字符串进行替换（也就是删除）.

replacement中可以包含后向引用\n 或(php 4.0.4以上可用)$n, 语法上首选后者，这样便可以在replacement里面直接使用捕获到的子组内容。
n 可以是0-99, \0和$0代表完整的模式匹配文本. 捕获子组的序号计数方式为: 代表捕获子组的左括号从左到右, 从1开始数。
如果引用后面还跟着数字，使用这样的方式来避免歧义：${1}1，如果字符串是在双引号里面，则使用\${1}1避免歧义。

如果要在replacement里面使用$字符本身，则转义：\$。如果双引号，$后面着数字即使转义了，也会当作引用对待。例如：”\$1a”被替换为第一个捕获的子组后面跟着a。而”\$a1″则替换为$a1三个字符，”$a1″的话，则$a会当成变量解析。

可见，{y:pattern和replacement都使用单引号字符串，尤其是在有向后引用的情况下，可以避免麻烦。}

当使用e修饰符时, 这个函数会转义一些字符(即:’, “, \和NULL)然后进行后向引用替换。
当这些完成后请确保后向引用解析完后没有单引号或双引号引起的语法错误(比如: ‘strlen(\’$1\’)+strlen(“$2″)’)。
确保符合PHP的字符串语法, 并且符合eval语法. 因为在完成替换后, 引擎会将结果字符串作为php代码使用eval方式进行评估并将返回值作为最终参与替换的字符串.

##### subject

要进行搜索和替换的字符串或字符串数组.

如果subject是一个数组, 则会对每一个元素都执行搜索和替换。(而不是和pattern,replacement一一对应), 并且返回值也会是一个数组.

##### limit

每个模式在每个subject上进行替换的最大次数. 默认是 -1(无限).

##### count

引用变量，如果指定, 将会保存完成的替换次数.


<p class="api">mixed preg_replac_callback(mixed $pattern,callback $callback,mixed $subject[,int $limit=-1[,int &$count]])</p>

与preg_replace的不同在于，它是通过回调函数进行替换，preg_mathc_all以PREG_SET_ORDER方式返回的数组中的每个元素(是一个数组，0号是完整匹配，1号是子组1….)依次传入回调函数，函数返回值作为这个匹配的替换结果。

<p class="api">mixed preg_filter(mixed $pattern,mixed $replacement,mixed $subject[,int $limit=-1[,int &$count]])</p>

与preg_replace的唯一不同在于，它只返回替换过的元素，没有发生替换的元素，不会在返回结果中出现。

```php
echo "<pre>";
$str    = 'this is a test string,for function TEST study';
$result = preg_replace( '/test/i', '测试',$str );
var_dump( $result );

$result = preg_replace( '/test/i', '测试',$str,1 );
var_dump( $result ); //使用第四个参数限制最多替换的次数，-1表示不限制

$result = preg_replace( '/test/i', '测试',$str,-1,$count );
var_dump( $result ,$count);//使用第五个参数记录替换的次数

$result = preg_replace( array('/test/','/TEST/'), '测试',$str );
var_dump( $result );

$result = preg_replace( array('/test/','/TEST/'), array('测试','功能'),$str );
var_dump( $result );

$result = preg_replace( array('/test/','/TEST/'), array('测试'),$str );
var_dump( $result );

$result = preg_replace( array('/te(s)t/','/T(E)(S)T/'), array('$1','$2'),$str );
var_dump( $result );//在替换中直接使用子组

$result = preg_replace( array('/te(s)t/','/T(E)(S)T/'), array('\$1','${2}1'),$str );
var_dump( $result );//转义和避免子组歧义

$result = preg_filter( array('/test/','/TEST/'), array('测试','功能'),array('testhere','nothing','TESTHERE') );
var_dump( $result );//返回的数组中，只含有替换过的元素（php>=5.3）
$result = preg_replace( array('/test/','/TEST/'), array('测试','功能'),array('testhere','nothing','TESTHERE') );
var_dump( $result );//返回的数组中，含有替换过的元素，也含有没有替换的元素

//使用回调函数进行替换
$result = preg_replace_callback( '/(t)e(s)t/', 'toupper',$str );
var_dump( $result );
function toupper($arr){
    return strtoupper($arr[0]).'+'. strtoupper($arr[1]).strtoupper($arr[2]);
}

echo "</pre>";
```

<p class="api">array preg_grep(string $pattern,array $input[,int $flags=0])</p>

用一个正则表达式过滤数组，默认返回数组中与pattern 匹配的元素组成的数组；

!!!等效于preg_filter($pattern, ‘$0′,array $input );

flags设为`PREG_GREP_INVERT`，则只返回没有匹配的元素。

如果要用多个正则过滤数组，可以用<span class="api">array preg_filter(array($pattern,$pattern2…),array(‘$0′,’$0′,…),array $input)</span>

<p class="api">array preg_split(string $pattern,string $subject[,int $limit=-1[,int $flags=0]])</p>

与explode函数作用相同，但本函数可以通过一个正则表达式分隔给定字符串. 

<p class="api">int preg_last_error(void)</p>

#####返回下面常量中的一个(查看它们自身的解释):

* PREG_NO_ERROR
* PREG_INTERNAL_ERROR
* PREG_BACKTRACK_LIMIT_ERROR （参见 pcre.backtrack_limit）
* PREG_RECURSION_LIMIT_ERROR （参见 pcre.recursion_limit）
* PREG_BAD_UTF8_ERROR
* PREG_BAD_UTF8_OFFSET_ERROR （自 PHP 5.3.0 起）

<p class="api">string preg_quote(string $str[,string $delimiter=NULL])</p>

preg_quote()需要参数 str 并向其中 每个正则表达式语法中的字符前增加一个反斜线。 这通常用于你有一些运行时字符串 需要作为正则表达式进行匹配的时候。

正则表达式特殊字符有： . \ + * ? [ ^ ] $ ( ) { } = ! < > | : -