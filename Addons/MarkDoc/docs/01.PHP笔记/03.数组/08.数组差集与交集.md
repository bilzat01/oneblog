 
数组求差集
=====================

## 内置实现

> 元素执行的是**松比较**。且不支持对象元素(除非对象有__toString)
>
> 返回的元素的字符串键名和数值**索引均保留不变**

!!!!! 只支持一维数组

<p class="api">
array array_diff ( array $array1 , array $array2 [, array $ ... ] )
</p>

根据value进行比较，返回一个数组，该数组包括了所有在 array1 中但是不在任何其它参数数组中的元素。

<p class="api">
array array_diff_key ( array $array1 , array $array2 [, array $ ... ] )
</p>

与array_diff()唯一的不同是它比较的是键名

<p class="api">
array array_diff_assoc ( array $array1 , array $array2 [, array $ ... ] )
</p>

它不仅比较键名，也比较键值。


## 自定义实现

> 通过回调函数，实现自定义的元素比较机制

回调函数参数

:	两个参数，第一个参数接收前面的数组传入的元素，第二个参数接收后面的数组传入的参数

回调函数执行过程

:	从第一个数组取出valueA1,从第二个数组中取出元素valueB1进行比较。

:	如果比较后回调函数返回值不为0，则valueA1与第二个数组的下一个元素valueB2进行比较。

:	如果回调函数返回值为0，表示这两个元素相同，valueA1不出现在结果数组中，开始用第一个数组的下一个元素valueA2比较与valueB1进行比较。

!!!!!注意：返回的元素的字符串键名和数值索引均保留不变 

array_udiff 应用回调函数对value进行比较求得元素差集

array_diff_ukey 应用回调函数对key进行比较求得元素差集

array_diff_uassoc 应用回调函数对key进行比较,应用内置机制对value进行比较求得元素差集

array_udiff_assoc 应用回调函数对value进行比较,应用内置机制对key进行比较求得元素差集

array array_udiff_uassoc ( array $array1 , array $array2 [, array $ ... ], callable $data_compare_func , callable $key_compare_func )

对value和key都应用回调函数进行比较，需两个回调函数


```php
//删除数组中的某个对象的所有引用
array_udiff ( $array, array($obj), function($a,$b){return ($a===$b)?0:1;} ) 
```

数组求交集
================


内置实现交集
--------

> 元素执行的是**松比较**。且不支持对象元素(除非对象有__toString)
>
> 返回的元素的字符串键名和数值**索引均保留不变**

!!!!! 只支持一维数组

array array_intersect ( array $array1 , array $array2 [, array $ ... ] )

根据value进行比较

array_intersect_key()

根据key进行比较

array_intersect_assoc()

根据value和key进行比较
 
返回一个数组，该数组包含了在所有参数数组中都存在的元素。


自定义实现交集
----------

> 通过回调函数，实现自定义的元素比较机制

回调函数参数

:	两个参数，第一个参数接收前面的数组传入的元素，第二个参数接收后面的数组传入的参数

回调函数执行过程

:	从第一个数组取出valueA1,从第二个数组中取出元素valueB1进行比较。

:	如果比较后回调函数返回值不为0，则valueA1与第二个数组的下一个元素valueB2进行比较。如果依次将数组2的元素都测试完依然没有找到与valueA1相等的，则这个元素不出现在返回结果中。

:	如果回调函数返回值为0，表示这两个元素相同，valueA1出现在结果数组中，开始用第一个数组的下一个元素valueA2比较与valueB1进行比较。

注意：返回的元素的字符串键名和数值索引均保留不变
 
array_uintersect() 对值应用回调函数

array_intersect_ukey() 对键应用回调函数

array_uintersect_assoc() 对值应用回调函数， 对键应用内置函数

array_intersect_uassoc() 对值应用内置的函，对键应用回调函数

array_uintersect_uassoc() 对值和键都应用回调函数，需两个回调函数