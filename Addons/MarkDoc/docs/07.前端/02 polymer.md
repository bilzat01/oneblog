# 起步

开始之前，先下载[起步项目](http://www.videolan.org/developers/x265.html)

剩下的见官方示列

# Polymer

## 开发者api指南

### 元素声明
Polymer的核心是自定义元素。因此，定义一个Polymer元素和定义一个标准自定义元素相似就不会那么令人吃惊。主要区别是用`<polymer-element>`来创建声明。

	<polymer-element name="tag-name" constructor="TagName">
	  <template>
	    <!-- shadow DOM here -->
	  </template>
	  <script>Polymer('tag-name');</script>
	</polymer-element>

#### 属性

Polymer 允许特殊的属性来用在 `<polymer-element>`上

| 属性        | 必填？           | 描述  |
| ------------- |:-------------:| -----:|
| name      | *required* | 自定义元素的名称。必须有一个‘-’ |
| attributes      | 可选      |   用于公共属性 |
| extends | 可选     |    用于扩展属性 |
| noscript|可选 	 | 用于简单元素不需要调用 `Polymer()` 见另外一种方式注册一个元素|
| constructor| 可选 | 构造器的名称用于放置全局对象。允许用户来创建你元素的实例（e.g. `var tagName = new TagName()`）。 |

##### 默认属性
你在`<polymer-element>`上声明的其他属性将自动包含在每个实例上。举个例子：

	<polymer-element name="tag-name" class="active" mycustomattr>
	  <template>...</template>
	  <script>Polymer('tag-name');</script>
	</polymer-element>
##### 属性区分大小写
没什么疑惑的HTML解释器认为属性名不区分大小写。然而Javascript里是区分大小写的。

这意味着属性可以被写成你喜欢的如何方式，但是如果你看到元素属性列表，名称总是小写的。Polymer注意到这点，并且试图小心的匹配属性。举个例子，下面的预期是可以工作的：

	<name-tag nameColor="blue" name="Blue Name"></name-tag>

事实上 `nameColor`属性在DOM中实际是小写的，可以完全忽略。

这也意味着下面例子中的所有都是正确可行的：

	<name-tag NaMeCoLoR="blue" name="Blue Name"></name-tag>
	<name-tag NAMECOLOR="red" name="Red Name"></name-tag>
	<name-tag NAMEcolor="green" name="Green Name"></name-tag>

#### 另外一种方式注册一个元素
为了方便给脚本和标记去耦，你不必代码中写这些属性。Polymer元素可以引入一个内部脚本调用了`Polymer('tag-name')`：

	<!-- 1. Script referenced inside the element definition. -->
	<polymer-element name="tag-name">
	  <template>...</template>
	  <script src="path/to/tagname.js"></script>
	</polymer-element>
	
	<!-- 2. Script comes before the element definition. -->
	<script src="path/to/tagname.js"></script>
	<polymer-element name="tag-name">
	  <template>...</template>
	</polymer-element>
	
	<!-- 3. No script -->
	<polymer-element name="tag-name" constructor="TagName" noscript>
	  <template>
	    <!-- shadow DOM here -->
	  </template>
	</polymer-element>

**紧急注册**

元素可以被用纯Javascript 注册比如：

	<script>
	  Polymer('name-tag', {nameColor: 'red'});
	  var el = document.createElement('div');
	  el.innerHTML = '\
	    <polymer-element name="name-tag" attributes="name">\
	      <template>\
	        Hello <span style="color:{{nameColor}}">{{name}}</span>\
	      </template>\
	    </polymer-element>';
	  // The custom elements polyfill can't see the <polymer-element>
	  // unless you put it in the DOM.
	  document.body.appendChild(el);
	</script>
	
	<name-tag name="John"></name-tag>

注意 你需要添加`<polymer-element>`到文档中，这样，自定义的元素，填充会拾起它。

#### 添加公共属性和方法

如果你想要定义方法/属性 在你的元素上（可选），传一个对象作为第二参数到`Polymer()`。这个对象用于定义元素的`prototype`。

下面的例子定义一个属性`message`，一个计算过的属性`greeting` 使用一个ES5 geeter,和一个 方法 `foo`:

	<polymer-element name="tag-name">
	  <template></template>
	  <script>
	    Polymer('tag-name', {
	      message: "Hello!",
	      get greeting() {
	        return this.message + ' there!';
	      },
	      foo: function() {...}
	    });
	  </script>
	</polymer-element>
	

> 注意： `this`引用了在一个Polymer元本身。举个例子，`this.localName == 'tag-name'`

**重要的**： 小心初始化对象或数组的属性。由于`prototype`的天性，你也许会进入同一元素的跨实例的未预期的“共享状态”。如果你初始化一个数组或对象，在创建的回调中执行，不要直接在原型上做。

	// Good!
	Polymer('x-foo', {
	  created: function() {
	    this.list = []; // Initialize and hint type to be array.
	    this.person = {}; // Initialize and hint type to an object.
	  }
	});
	
	// Bad.
	Polymer('x-foo', {
	  list: [], // Don't initialize array or objects on the prototype.
	  person: {}
	});
	Adding private or static variables
	If you need private state within an element, wrap your script using standard techniques like anonymous self-calling functions:
	
	<polymer-element name="tag-name">
	  <template>...</template>
	  <script>
	    (function() {
	      // Ran once. Private and static to the element.
	      var foo_ = new Foo();
	
	      // Ran for every instance of the element that's created.
	      Polymer('tag-name', {
	        get foo() { return foo_; }
	      });
	    })();
	  </script>
	</polymer-element>

#### 添加公有或静态变量

#### 支持全局变量

#### polymer-ready 事件

### 特性

#### 发布属性

#### 数据绑定和发布属性

#### 陈述性事件映射

#### 观察属性

#### 自动节点查找

#### 触发自定义事件

#### 扩展其他元素

### 内建元素方法

#### 观察少量DOM子节点的改变

#### 处理异步任务

#### 延迟工作

### 高级主题

#### 一个元素绑定的周期

#### 数据变化如何存储的

#### 如何Polymer元素自我准备

#### 解决同级元素的路径