输出重定向
===========

> 指不把输出结果显示在标准输出设备上，而输出到文件中

* `>`    输出重定向符号（清空文件从头开始写入）
* `>>`  追加输出重定向（输出内容追加到文件尾部）
* `2>`  错误输出重定向，如果发生错误，执行输出重定向，没发生错误，只清空用于接收输出内容的文件
* `2>>` 追加错误输出重定向（输出内容追加到文件尾部）

管道
=====

> 把一个命令的输出作为另一个命令的输入 |

`$ ls -l | more`  分页显示ls命令的结果
`$ ls -l | grep init ` 在ls命令的输出结果中查找含有init内容的内容
`$ ls -l | grep init |wc -l`
`$ tar -cvf - /home | tar -xvf -` 把 /home 里面的文件给他打包，但打包的数据不是纪录到文件，而是传送到 stdout； 经过管线后，将 tar -cvf - /home 传送给后面的 tar -xvf - 』。后面的这个 - 则是取用前一个命令的 stdout！


命令连接符
=======

* `;`  符号   例：`$ 命令1 ; 命令2; 命令3...` 命令依次执行，不去管执行是否成功
* `&&` 逻辑与 `$ 命令1 && 命令2`   命令1执行成功后继续执行第二个命令；命令1执行失败，命令2不执行。
* `||` 逻辑或 命令1可以执行成功的话，命令2就不需要执行了，否则去执行第二个命令
* \`(反引号)：命令替换符，把一个命令的输出作为另一个命令的参数.例如：{g:$ls -l \`which touch\`}

特殊设备
==========

`/dev/null` 垃圾桶黑洞

`/dev/zero` 是一个特殊的文件，当你读它的时候，它会提供无限的空字符


实用命令
=========

xargs
-----

该命令可以依次读取输入内容的行,每一行都单独作为参数传给另一个命令执行

维基百科:<http://zh.wikipedia.org/wiki/Xargs>

技巧
=====

1. tab键 命令和文件目录自动补全, 如果可能有多个匹配，第一次tab没反应，第二次会显示出所有可能的结果

2. 别名:`alias` 不加参数显示所有别名
    * `$ alias ls="ls -l --color=auto"`
    * 取消别名 `unalias`

3. `\ +回车`  断行继续输入命令

4. 快捷键
    * Ctrl + C 终止目前的命令
    * Ctrl + D 输入结束 (EOF)，例如邮件结束的时候；
    * Ctrl + M 就是 Enter 啦！
    * Ctrl + S 暂停屏幕的输出
    * Ctrl + Q 恢复屏幕的输出
    * Ctrl + U 在提示字符下，将整列命令删除
    * Ctrl + Z 『暂停』目前的命令

5. 通配符与特殊符号
    * `*` 代表『 0 个到无穷多个』任意字符
    * `?` 代表『一定有一个』任意字符
    * `[ ]` 同样代表『一定有一个在括号内』的字符(非任意字符)。例如 [abcd] 代表『一定有一个字符， 可能是 a, b, c, d 这四个任何一个』
    * `[ - ]` 若有减号在中括号内时，代表『在编码顺序内的所有字符』。例如 [0-9] 代表 0 到 9 之间的所有数字，因为数字的语系编码是连续的！
    * `[^ ]` 若中括号内的第一个字符为指数符号 (^) ，那表示『反向选择』，例如 [^abc] 代表 一定有一个字符，只要是非 a, b, c 的其他字符就接受的意思。
