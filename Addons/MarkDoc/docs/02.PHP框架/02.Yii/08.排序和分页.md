## CPagination

> 提供数据分页查询功能，它可以为查询设定用户GET请求的页码，以使查询能返回预期的数据,并为widget提供的初始化配置提供需要的CPagination对象。

#### 原理

> CPagination实例的属性中保存了各种分页信息：items总数，每页items数量，页数，当前的页码。都可以通过get方法得到。<br>
> 并且提供了一个在控制器中调用的方法applyLimit()用于修改criteria对象中的limit和offset条件，以供模型对象查询到需要的items。<br>
> 为视图和widget提供了创建分页链接的createPageUrl()方法，创建的链接中默认通过GET变量page来传递请求的页码。


#### 配置属性

+ $route=''  分页链接使用的route,默认为空，表示使用当前请求的route。如果做ajax分页，需要设置返回数据的action的route
+ $pageVar='page'  用来表示请求的页码的GET变量
+ $params  额外的GET变量，默认为空数组
+ $pageSize  每页显示的items数量,默认为10
+ $itemCount items总数，默认为0，如果不是data provider方式调用，必须配置该属性。
+ $currentPage 当前的页码，如果你不希望默认显示第一页，可以配置此项

#### 主要方法：

__construct($itemCount=0)

+ applyLimit($criteria) 
	- 根据当前的页数和每页的items数，为criteria设置正确的limit和offset，以执行查询

基本流程： 
```php
<?php
public function actionIndex(){
    //setp 1:创建criteria
    $criteria=new CDbCriteria();
    //step 2:统计criteria可以查询得到的条目总数
    $count=Article::model()->count($criteria);
    //STEP 3:创建CPagination实例
    $pages=new CPagination($count);
    //STEP 4:配置CPagination实例
    $pages->pageSize=10;
    //STEP 5:applyLimit
    $pages->applyLimit($criteria);
    //STEP 6:执行查询渲染视图
    $models=Article::model()->findAll($criteria);
    $this->render('index', array(
    	 'models' => $models,//将查询到的变量赋值到视图
         'pages' => $pages//将CPagination实例赋值到视图
    ));
}
```
```php
<?php foreach($models as $model): ?>
    // 遍历记录
<?php endforeach; ?>

// 显示分页链接
<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
)) ?>
```

## CSort

> 提供数据排序查询功能，它可以为查询设定用户GET请求的排序方式,并为widget提供的初始化配置提供需要的CSort对象。

#### 原理

> 提供了一个在控制器中调用的方法applyOrder()用于修改criteria对象中的order条件，以供模型对象查询到需要的items。<br>
> 为视图和widget提供了link()方法创建特性排序链接。

#### 配置属性

+ $attributes=array(); 
	
	- 进行排序的模型特性和排序配置，默认为空表示所有特性按降序排序。
	
	- 如果模型特特性作为索引元素提供，则按该特性排序；你也可以使用一个虚拟的特性名称作为key,在value中为其定义更加复杂灵活的排序方式。
	 
		例如, array('user_id','create_time') 指定了按照模型的 'user_id' 和'create_time' 进行排序。
	
		array('user'=>'user_id') 则指定了按照模型的user_id进行排序，虚拟的特性名称是user

	- 如果虚拟特性由多个特性组成，你需要使用数组配置虚拟特性的实际特性，数组的元素的key分别是asc,desc,label,default(可选)：
	
			'user'=>array(
				'asc'=>'first_name, last_name',
				'desc'=>'first_name DESC, last_name DESC',
				'label'=>'Name'
			 )

		上面的配置方式也可以用于关联表的字段，例如：
		
			 'price'=>array(
				 'asc'=>'item.price',
				 'desc'=>'item.price DESC',
				 'label'=>'Item Price'
			 )

 		注意，特性名称不要含有 '-' 和 '.' 符号，因为默认他们被用作分割符

		'default'用于配置在未排序情况下用户点击排序链接时虚拟字段的排序方式

			'price'=>array(
				 'asc'=>'item.price',
				 'desc'=>'item.price DESC',
				'label'=>'Item Price',
				 'default'=>'desc',
			)
			
+ $multiSort=false; 是否对多重字段排序同步执行，默认为否，表示一个字段排序完成后再排序下一个字段。

+ $sortVar='sort'; 用来指明排序的GET变量名,url中它的变量值用来指明排序的升降序

+ $descTag='desc'; 用来表示降序的 sortVar的变量值，默认为 'desc'

+ $params  额外的GET变量

+ $separators=array('-','.'); 数组有两个元素，第一个元素用来分割特性，第二个特性用来分割特性和它的排序方式。

+ $route=''; 该route用来生成排序后的内容，默认为空，表示使用当前请求的route

+ $defaultOrder; 

	如果查询没有指定排序方式，那么使用这里的条件作为criteria的order条件。 For example, 'name, create_time DESC'
	
	你也可以使用数组，key是模型特性或者虚拟特性的名称，value是为true表示降序，为false表示升序 'defaultOrder'=>array( 'price'=>CSort::SORT_DESC, )


#### 主要方法

+ **__construct**($modelClass=null)
+ **applyOrder($criteria)**  修改查询criteria对象中的排序条件，以执行排序查询。
+ **link**($attribute,$label=null,$htmlOptions=array())
	- $attribute 特性名称，必须是真实的AR模型特性名称，关联查询使用表名前缀而不能使用alias。

Data providers 
==============

> 排序和分页二合一的Data providers， 它可以为视图提供分页和排序以后的数据,并为widget提供的初始化配置提供需要的CSort,CPagination对象。


## CDataProvider 

> CDataProvider 是所有data provider的基类

#### 配置属性
 
 * $id  data provider的唯一ID
 * $pagination 配置分页对象，可以是一个配置数组，也可以是CPagination对象，如果不配置，表示不分页
 * $sort 配置排序对象，可以是一个配置数组，也可以是CSort对象,如果不配置，表示不排序。

#### 主要方法

+ getData() 根据排序和分页的要求查询数据并以数组返回这些数据的itmes，每个item是数组的一个元素，item的类型数据源或查询方式决定。
+ getKeys() 根据排序和分页的要求查询数据并以数组返回这些数据的的keys
+ getPagination($className='CPagination') 返回分页对象(提供给分页widget使用)
+ getSort($className='CSort') 返回排序对象(提供给排序widget使用)
+ getItemCount() 返回本次查询到的itmes的数量(也就是本页的items数量)
+ getTotalItemCount() 返回总的itmes的数量

## CArrayDataProvider

> 使用模型或DAO查询返回的rawData数组创建data provider

rawData数组的元素既可以是DAO返回的关联数组也可以是AR返回的AR对象。

#### 配置属性

 * $keyField='id'  使用哪个字段的value作为items的key,默认使用id字段。必须是一个唯一型字段。
 * $caseSensitiveSort=true  排序比较的时候是否大小写敏感,默认为true

__construct($rawData,$config=array())

```php
<?php
  ...
  $rawData=Yii::app()->db->createCommand('SELECT * FROM tbl_user')->queryAll();
  
  $dataProvider=new CArrayDataProvider($rawData, array(
      'id'=>'user',
      'sort'=>array(
          'attributes'=>array(
               'id', 'username', 'email',
          ),
      ),
      'pagination'=>array(
          'pageSize'=>10,
      ),
  ));
  $users = $dataProvider->getData();
  
  $this->render('list',array(
  			'users'=>$users,
  			'pagination'=>$dataProvider->getPagination(),
  			'sort'=>$dataProvider->getSort(),
  		));
```

## CActiveDataProvider

> 使用AR模型创建data provider，在内部自动执行需要的查询，更加简便。

#### 配置属性

 * $criteria 配置查询条件，根据这个条件生产查询是需要的CCriteria对象
 * $keyAttribute 使用哪个模型特性作为items的key,默认使用表的主键字段。必须是一个唯一型模型特性。

**__construct($modelClass,$config=array())**

* $modelClass 可以是AR实例，也可以是AR类名

```php
<?php
  ...
  $dataProvider=new CActiveDataProvider('Post', array(
      'criteria'=>array(
          'condition'=>'status=1',
          'order'=>'create_time DESC',
          'with'=>array('author'),
      ),
      'pagination'=>array(
          'pageSize'=>20,
      ),
  ));
  $items = $dataProvider->getData();
```
## CSqlDataProvider 

> 通过sql语句创建data provider,适用于没有模型的查询。

#### 配置属性

* $db 默认使用db组件
* $params=array()  如果是预处理sql语句，用它配置绑定的参数
* $keyField='id'  使用哪个字段的value作为items的key,默认使用id字段。必须是一个唯一型字段。
* $totalItemCount  如果你想使用分页，必须用一条统计查询计算出items总数配置给它


**__construct($sql,$config=array())**


```php
<?php
  ...
  $count=Yii::app()->db->createCommand('SELECT COUNT(*) FROM tbl_user')->queryScalar();
  $sql='SELECT * FROM tbl_user';
  $dataProvider=new CSqlDataProvider($sql, array(
      'totalItemCount'=>$count,
      'sort'=>array(
          'attributes'=>array(
               'id', 'username', 'email',
          ),
      ),
      'pagination'=>array(
          'pageSize'=>10,
      ),
  ));
  // $dataProvider->getData() will return a list of arrays.
```

## CDataProviderIterator 

> 如果你想要一次发送所有的items到浏览器，但是分页显示，用它最好。它允许迭代处理大数据，而无需将所有数据一次载入内存。<br>
> 遍历的内容是一页中的items，每遍历一次页码数量+1

+ __construct(CDataProvider $dataProvider, $pageSize=null)
+ count()返回的是总的items数量
+ getDataProvider()