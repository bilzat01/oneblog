 
# errorHandler 组件

>在开发YII_DEBUG常量为true的开发模式，Yii可以捕捉运行中发生的错误和未捕获异常，并显示出详细的Stack Trace信息。

>在部署模式下，则会显示一个500页面，只显示未捕获的异常的message。


* 要获得当前的错误信息数组，调用

        Yii::app()->errorHandler->getError();
        =====================================
        code: HTTP 状态代码 (例如 403, 500);
        type: 错误类型 (例如 CHttpException, PHP Error);
        message: 错误信息;
        file: 出现错误的 PHP 脚本的名字;
        line: 出现错误的代码行数;
        trace : 错误的调用栈;
        source: 错误出现的源代码片段。


* 要手动触发一个错误或异常，调用

        Yii::app()->errorHandler->handle(CEvent $event);

    参数如果是:`CErrorEvent`实例则触发错误，如果是`CExceptionEvent`实例则触发异常

    在触发错误之前，可以改变`$errorAction`属性，指定一个控制器来显示内容。

* CErrorHandler 以以下顺序寻找一个视图的视图文件:
    1. WebRoot/themes/ThemeName/views/system : 这是当前激活的主题的 system 视图目录。
    2. WebRoot/protected/views/system : 这是一个应用的默认的 system 视图目录。
    3. yii/framework/views: 这是由 Yii 框架提供的标准系统视图目录。


## securityManage 组件

> 它提供基于私匙的hash和加密功能。例如:Yii用这个组件验证cookie,防止cookie遭篡改

#### 属性
ValidationKey  用于HMAC验证的key,可以在配置中手动指定，不指定的话可以自动随机生成。
EncryptionKey  用于加密解密的key,可以在配置中手动指定，不指定的话可以自动随机生成。

#### 方法
* encrypt($data,$key=null)  加密数据
* decrypt($data,$key=null)  解密数据
* hashData($data,$key=null) 生成数据的hash值
* validateData($data,$key=null) 验证数据是否被篡改

## request 组件

> 提供 解析http请求，cookie安全,csrf防范，向客户端发送文件

> 提供了大量get方法用来获取请求中的所有类型的信息。

<table style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>
<tbody>
<tr>
	<td style="" ><strong style="text-align: center;">cookie读写属性</strong></td>
	<td style="" ><strong style="text-align: center;">说明</strong></td>
</tr>

<tr>
	<td style="" >$cookies</td>
	<td style="" >CCookieCollection实例，可以数组式访问和修改，可以遍历，统计</td>
</tr>
<tr>
	<td style="" ><strong style="text-align: center;">配置属性</strong></td>
	<td style="" ></td>
</tr>
<tr>
	<td style="" >$enableCookieValidation</td>
	<td style="" >默认为false 是否对cookie进行验证</td>
</tr>

<tr>
	<td style="" >$enableCsrfValidation=false</td>
	<td style="" >是否进行csrf攻击防范，如果启用此项，需使用CHtml::form或者 CHtml::statefulForm在视图创建表单</td>
</tr>

<tr>
	<td style="" >$csrfTokenName='YII_CSRF_TOKEN'</td>
	<td style="" >csrf使用的标识名称</td>
</tr>

<tr>
	<td style="" ><strong style="text-align: center;">文件下载</strong></td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >sendFile($fileName,$content)</td>
	<td style="" >echo的方式发送文件下载</td>
</tr>

<tr>
	<td style="" >xSendFile($filePath, $options=array())</td>
	<td style="" >x-sendfile方式发送文件下载。需web服务器支持x-sendfile功能</td>
</tr>

<tr>
	<td style="" ><strong style="text-align: center;">超全局数组读取</strong></td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getQuery($name,$defaultValue=null)</td>
	<td style="" >$_GET[$name]</td>
</tr>



<tr>
	<td style="" >getPost($name,$defaultValue=null)</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getDelete($name,$defaultValue=null)</td>
	<td style="" >v1.1.7</td>
</tr>

<tr>
	<td style="" >getPut($name,$defaultValue=null)</td>
	<td style="" >v1.1.7</td>
</tr>

<tr>
	<td style="" >getParam($name,$defaultValue=null)</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getRestParams()</td>
	<td style="" ></td>
</tr>


<tr>
	<td style="" ><strong style="text-align: center;">请求类型判断</strong></td>
	<td style="" ></td>
</tr>


<tr>
	<td style="" >getRequestType()</td>
	<td style="" >v1.1.11 GET, POST, HEAD, PUT, DELETE.</td>
</tr>

<tr>
	<td style="" >getIsAjaxRequest()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getIsFlashRequest()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getIsPostRequest()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getIsDeleteRequest()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getIsDeleteViaPostRequest()</td>
	<td style="" >v1.1.11</td>
</tr>

<tr>
	<td style="" >getIsPutRequest()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getIsPutViaPostRequest()</td>
	<td style="" >v1.1.11</td>
</tr>

<tr>
	<td style="" >getIsSecureConnection()</td>
	<td style="" >是否https请求</td>
</tr>

<tr>
	<td style="" ><strong style="text-align: center;">客户端信息</strong></td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getUserAgent()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getUserHostAddress()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getUserHost()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getBrowser($userAgent=null)</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getAcceptTypes()</td>
	<td style="" ></td>
</tr>


<tr>
	<td style="" ><strong style="text-align: center;">URL信息</strong></td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" ><strong style="text-align: center;">redirect($url,$terminate=true,$statusCode=302)</strong></td>
	<td style="" >URL重定向，支持绝对url(以"/"开头)和相对url(根据当前请求补全)</td>
</tr>

<tr>
	<td style="" >getHostInfo($schema='')</td>
	<td style="" >返回请求的主机的网址  (e.g. http://www.yiiframework.com)，末尾不是"/".</td>
</tr>

<tr>
	<td style="" >getBaseUrl($absolute=false)</td>
	<td style="" >返回入口脚本服务器域目录url</td>
</tr>

<tr>
	<td style="" >getScriptUrl()</td>
	<td style="" >返回服务器域的入口脚本文件url，不含任何参数</td>
</tr>

<tr>
	<td style="" >getPathInfo()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getQueryString()</td>
	<td style="" >$<em>SERVER['QUERY</em>STRING']</td>
</tr>


<tr>
	<td style="" >getRequestUri()</td>
	<td style="" ></td>
</tr>

<tr>
	<td style="" >getScriptFile()</td>
	<td style="" >$_SERVER['SCRIPT_FILENAME']</td>
</tr>

<tr>
	<td style="" >getUrlReferrer()</td>
	<td style="" >$_SERVER['HTTP_REFERER']</td>
</tr>

</tbody>
</table>

### CHttpCookie > CComponent

<table style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>


	<tbody>
	<tr>
<td style="" ><strong>属性<／strong></td>
		<td style="" >$name</td>

		<td style="" >$value=''</td>

		<td style="" >$domain=''</td>

		<td style="" >$expire=0</td>
	
		<td style="" >$path='/'</td>
	
		<td style="" >$secure=false</td>

		<td style="" >$httpOnly=false</td>
		
	</tr>

	<tr>
		<td style="text-align: center;" ><strong>方法</strong></td>

		<td colspan="7" style="" >__construct($name,$value,$options=array())</td>
		
	</tr>

	</tbody>
</table>

## urlManager

> 提供了 路由管理，url创建，url解析功能

#### 规则示例1：

```php
'rules'=>array(
    'posts'=>'post/list',
    'post/<id:\d+>'=>'post/read',
    'post/<year:\d{4}>/<title>'=>'post/read',
)
```

    访问：/index.php/post/100时，第二条规则匹配，解析成r=post/read&id=100，可以获得$_GET['id']值为100
    访问：/index.php/post/2008时，第3条规则匹配，解析成r=post/read&year=2008，可以获得$_GET['year']值为2008
    调用 $this->createUrl('post/read',array('id'=>100)) 将生成‘/index.php/post/100’

#### 规则示例2：

```php
array(
    '<_c:(post|comment)>/<id:\d+>/<_a:(create|update|delete)>' => '<_c>/<_a>',
    '<_c:(post|comment)>/<id:\d+>' => '<_c>/read',
    '<_c:(post|comment)>s' => '<_c>/list',
)
```

    如果访问： index.php/posts 将匹配第三条规则,<_c>值为post在后面得到引用
    如果访问：index.php/comment/10，将匹配第二条规则。<_c>值为comment
    如果访问：index.php/post/10/update 将匹配第一条规则。<_c>为post，<_a>为update

#### 隐藏index.php

配置urlManager 组件的 showScriptName 属性为 false，并创建下面的rewrite规则：

```
Options +FollowSymLinks
IndexIgnore */*
RewriteEngine on
# if a directory or a file exists, use it directly
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
# otherwise forward it to index.php
RewriteRule . index.php
```

#### 创建url

```php
createUrl($route,$params=array(),$ampersand='&')
```