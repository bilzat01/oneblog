 
# db组件

## CDbConnection

> 主要进行数据库连接配置和管理、PDO属性设置、创建CDbCommand对象实例。

<table style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>
	<thead>
	<tr>
		<th width="auto" >配置属性</th>
		<th width="auto" >功能</th>
	</tr>
	</thead>

	<tbody>
	<tr>
		<td>$pdoClass = 'PDO'</td>
		<td>PDO类的类名或扩展自PDO的类名</td>
	</tr>

	<tr>
		<td>$connectionString</td>
		<td>例如<code>mysql:dbname=mydatabase;host=127.0.0.1;charset=GBK;</code></td>
	</tr>

	<tr>
		<td>$charset</td>
		<td>5.3.6+建议在dsn中指定，更安全</td>
	</tr>

	<tr>
		<td>attributes</td>
		<td>数组，配置db连接的属性，可配置项见PDO::setAttribute()</td>
	</tr>

	<tr>
		<td>$autoConnect=true</td>
		<td>默认为true</td>
	</tr>

	<tr>
		<td>$username</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>$password</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>$tablePrefix</td>
		<td>数据表前缀</td>
	</tr>

	<tr>
		<td>$schemaCacheID='cache'</td>
		<td>metadata缓存使用的缓存组件</td>
	</tr>

	<tr>
		<td>$schemaCachingDuration=0</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>$schemaCachingExclude=array()</td>
		<td>配置不需要缓存metadata的数据表，通常在开发阶段配置</td>
	</tr>

	<tr>
		<td>$queryCacheID='cache'</td>
		<td>查询缓存使用的缓存组件</td>
	</tr>

	<tr>
		<td>$queryCachingDuration=0</td>
		<td>v1.1.7</td>
	</tr>

	<tr>
		<td>$queryCachingCount=0</td>
		<td>缓存条目的数量，默认为0，表示不缓存查询</td>
	</tr>

	<tr>
		<td>$emulatePrepare</td>
		<td>默认为false。是否使用php来执行预处理，而不在数据库执行预处理。mysql可以设为true以提高性能</td>
	</tr>

	<tr>
		<td>$enableParamLogging=false</td>
		<td>是否log绑定到预处理语句的值。默认为false，开发阶段可以设为true以方便调试</td>
	</tr>

	<tr>
		<td>$enableProfiling=false</td>
		<td>默认为false。是否开启sqlstatements的profile，设为true在开发阶段用于分析性能瓶颈。</td>
	</tr>

	<tr>
		<td>$initSQLs</td>
		<td>数组，用来配置一些用于初始化的sql语句，这些语句在db连接建立后马上执行</td>
	</tr>

	</tbody>
</table>

<table style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>
	<thead>
	<tr>
		<th width="auto" >方法</th>
		<th width="auto" >说明</th>
	</tr>
	</thead>

	<tbody>
	<tr>
		<td>__construct($dsn='',$username='',$password='')</td>
		<td>创建一个新的db连接</td>
	</tr>

	<tr>
		<td>getActive()</td>
		<td>返回连接是否是active状态</td>
	</tr>

	<tr>
		<td>setActive($bool)</td>
		<td>设置连接状态</td>
	</tr>

	<tr>
		<td>cache($duration, $dependency=null, $queryCount=1)</td>
		<td><code>v1.1.7</code>配置查询缓存的有效期、依赖和数量</td>
	</tr>

	<tr>
		<td>getPdoInstance()</td>
		<td>返回pdo实例,你可以使用它以原生的PDO方式操作数据库</td>
	</tr>

	<tr>
		<td>getAttribute($name)</td>
		<td>等同PDO::getAttribute($name)</td>
	</tr>

	<tr>
		<td>setAttribute($name,$value)</td>
		<td>等同PDO::setAttribute($name,$value)</td>
	</tr>

	<tr>
		<td>getAttributes()</td>
		<td><code>v1.1.7</code>数组返回所有已经设置的PDO attributes</td>
	</tr>

	<tr>
		<td>getLastInsertID($sequenceName='')</td>
		<td>等同PDO::lastInsertId()</td>
	</tr>

	<tr>
		<td>quoteColumnName($name)</td>
		<td>为列(包括表)加引号：table.colname变成<strong>`table`.`colname`</strong></td>
	</tr>

	<tr>
		<td>quoteTableName($name)</td>
		<td>为表名加反引号</td>
	</tr>

	<tr>
		<td>quoteValue($str)</td>
		<td>等同PDO::quote方法</td>
	</tr>

	<tr>
		<td>createCommand($query=null)</td>
		<td>返回当前连接的CDbCommand实例，参数可以是SQL语句，或数组定义的查询条件,也可以不提供参数，随后调用实例的方法。(<code>v1.1.6</code>)</td>
	</tr>

	<tr>
		<td>beginTransaction()</td>
		<td>创建并返回当前连接的CDbTransaction实例</td>
	</tr>

	<tr>
		<td>getStats()</td>
		<td>返回上一次sql执行后的状态信息，要使用此方法，enableProfiling必须为true</td>
	</tr>

	<tr>
		<td>getCurrentTransaction()</td>
		<td>返回当前连接的CDbTransaction实例，如果没有激活的事务，返回null</td>
	</tr>

	<tr>
		<td>$queryCachingDependency</td>
		<td>公共属性，设置查询缓存依赖CCacheDependency实例</td>
	</tr>

	</tbody>
</table>

## CDbCommand

> 构造sql语句，执行CURD

有以下特点：

* 自动对表名，列名使用反引号
* 自动执行引号转义
* 它是抽象的，支持多种类型的数据库

<table style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>
	<thead>
	<tr>
		<th width="auto" >方法</th>
		<th width="auto" >功能</th>
	</tr>
	</thead>

	<tbody>
	<tr>
		<td>__construct(CDbConnection $connection,$query=null)</td>
		<td>$query参数可以是SQL语句数组定义的查询条件,也可以不提供参数，随后调用实例的方法。(<code>v1.1.6</code>)</td>
	</tr>

	<tr>
		<td>reset()</td>
		<td>将CDbCommand的属性全部置空，并返回$this以重新使用</td>
	</tr>

	<tr>
		<td>getText()</td>
		<td>获得当前的sql语句</td>
	</tr>

	<tr>
		<td>setText($sql)</td>
		<td>设置当前的sql语句，并返回$this</td>
	</tr>

	<tr>
		<td>getPdoStatement()</td>
		<td>返回PDOStatement实例，使用PDOStatement原生方法处理数据</td>
	</tr>

	<tr>
		<td>bindValue($name, $value, $dataType=null)</td>
		<td>绑定value,并返回$this</td>
	</tr>

	<tr>
		<td>bindValues(array $values)</td>
		<td>批量绑定values,并返回$this</td>
	</tr>

	<tr>
		<td>query($params=array())</td>
		<td>返回查询结果的CDbDataReader实例</td>
	</tr>

	<tr>
		<td>queryAll($fetchAssociative=true,$params=array())</td>
		<td>返回查询结果的二维数组</td>
	</tr>

	<tr>
		<td>queryRow($fetchAssociative=true,$params=array())</td>
		<td>返回一行记录的数组</td>
	</tr>

	<tr>
		<td>queryScalar($params=array())</td>
		<td style="" >返回查询条件的第一行第一列的值，是一个字符串</td>
	</tr>

	<tr>
		<td>queryColumn($params=array())</td>
		<td style="" >返回第一列</td>
	</tr>

	<tr>
		<td>execute($params=array())</td>
		<td>执行查询，并返回受影响行数</td>
	</tr>

	<tr>
		<td>buildQuery(array $query)</td>
		<td><code>v1.1.6</code></td>
	</tr>

	<tr>
		<td>select($columns='*', $option='')</td>
		<td style="" >写法同sql里面一样，但不需要为表和列加反引号。例如：'tbl_user.id AS user_id'</td>
	</tr>

	<tr>
		<td>setDistinct($value)</td>
		<td style="" >布尔值</td>
	</tr>

	<tr>
		<td>from($tables)</td>
		<td style="" >例如：'tbl_user as u, public.tbl_profile as p'</td>
	</tr>

	<tr>
		<td>where($conditions, $params=array())</td>
		<td style="" >例如：where('id=:id1 or id=:id2', array(':id1'=>1, ':id2'=>2))</td>
	</tr>

	<tr>
		<td>join($table, $conditions, $params=array())</td>
		<td>inner join类型， $conditions是on子句的内容。例如：join('tbl profile', 'user id=id')</td>
	</tr>

	<tr>
		<td>leftJoin($table, $conditions, $params=array())</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>crossJoin($table)</td>
		<td>cross join</td>
	</tr>

	<tr>
		<td>naturalJoin($table)</td>
		<td>natural join</td>
	</tr>

	<tr>
		<td>group($columns)</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>having($conditions, $params=array())</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>order($columns)</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>limit($limit, $offset=null)</td>
		<td style="" ></td>
	</tr>

	<tr>
		<td>union($sql)</td>
		<td style="" >例如：union('select * from tbl profile')</td>
	</tr>

	<tr>
		<td>insert($table, $columns)</td>
		<td style="" >$columns是(colname=>value)数组</td>
	</tr>

	<tr>
		<td>update($table, $columns, $conditions='', $params=array())</td>
		<td style="" >$columns是(colname=>value)数组，$conditions是where子句内容</td>
	</tr>

	<tr>
		<td>delete($table, $conditions='', $params=array())</td>
		<td style="" >$conditions是where子句内容</td>
	</tr>

	</tbody>
</table>

实例：

```php
<?php
$users = Yii::app()->db->createCommand()->select('*')->from('tbl user')->queryAll();
$sql = Yii::app()->db->createCommand()->select('*')->from('tbl user')->getText();
```
由于一个command实例查询结束后会有五个属性被赋值，所以同一个command实例要再次使用之前必须执行reset()方法

```php
<?php
$command = Yii::app()->db->createCommand();
$users = $command->select('*')->from('tbl user')->queryAll();
$command->reset(); // 清除上次执行的所有结果
$posts = $command->select('*')->from('tbl user')->queryAll();
```
## CDbDataReader > CComponent + _Iterator_ + _Countable_

* CDbCommand::query()将返回它的实例
* 它只可以被遍历一次，不允许rewind()

<table width="100%" style='border-collapse: collapse;width:auto;' border='1' bordercolor='#ddd' cellpadding='6'>
	<thead>
	<tr>
		<th width="auto" >方法</th>
		<th width="auto" >功能</th>
	</tr>
	</thead>

	<tbody>
	<tr>
		<td>bindColumn($column, &amp;$value, $dataType=null)</td>
		<td>内部调用PDOStatement::bindColumn()</td>
	</tr>

	<tr>
		<td>setFetchMode($mode)</td>
		<td>内部调用PDOStatement::setFetchMode()</td>
	</tr>

	<tr>
		<td>read()</td>
		<td>内部调用PDOStatement::fetch()</td>
	</tr>

	<tr>
		<td>readColumn($columnIndex)</td>
		<td>内部调用PDOStatement::fetchColumn()</td>
	</tr>

	<tr>
		<td>readObject($className,$fields)</td>
		<td>内部调用PDOStatement::fetchObject()</td>
	</tr>

	<tr>
		<td>readAll()</td>
		<td>内部调用PDOStatement::fetchAll()</td>
	</tr>

	<tr>
		<td>nextResult()</td>
		<td>内部调用PDOStatement::nextRowset()</td>
	</tr>

	<tr>
		<td>close()</td>
		<td>内部调用PDOStatement::closeCursor()</td>
	</tr>

	<tr>
		<td>count()</td>
		<td>内部调用PDOStatement::rowCount()</td>
	</tr>

	<tr>
		<td>getColumnCount()</td>
		<td>内部调用PDOStatement::columnCount()</td>
	</tr>

	<tr>
		<td>getIsClosed()</td>
		<td style="" ></td>
	</tr>

	</tbody>
</table>

## CDbTransaction
* commit()
* rollback()


## CMysqlSchema>_CDbSchema_

> 主要预定义一些常用的字段数据类型字符串,重写_CDbSchema_类中的一些方法

```php
<?php
public $columnTypes=array(
'pk' => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
'string' => 'varchar(255)',
'text' => 'text',
'integer' => 'int(11)',
'float' => 'float',
'decimal' => 'decimal',
'datetime' => 'datetime',
'timestamp' => 'timestamp',
'time' => 'time',
'date' => 'date',
'binary' => 'blob',
'boolean' => 'tinyint(1)',
'money' => 'decimal(19,4)',
);
```