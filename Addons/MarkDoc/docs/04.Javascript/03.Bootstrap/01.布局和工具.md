# 网格系统
   
## 列定义 .span1 ~ .span12
>宽度为 `n*8-20` 例如：span5的宽度是：5*8-20=380px
```css
[class*="span"] {
    float: left;
    min-height: 1px;
    margin-left: 20px;
}
```
   
## 行定义 .row
   
```css
.row {
    margin-left: -20px;
}
.row::before, .row::after {
    display: table;
    line-height: 0;
    content: "";
}
.row::after {
    clear: both;
}
```
   
<div class="row" style="background:#eee">
  <div class="span4" style="background:#ddd">1行1列 span4</div>
  <div class="span5" style="background:#ddd">1行2列 span5</div>
</div>
<div class="row" style="background:#E2E2E2">
  <div class="span4" style="background:#C7C7C7">2行2列 span4</div>
  <div class="span5" style="background:#C7C7C7">1行2列 span5</div>
</div>
   
   
## 列偏移 .offset1 ~ .offset12
   
> margin-left的值为：n*80+20,例如：.offset1的偏移值为：100
   
<div class="row" style="background:#eee">
  <div class="span4" style="background:#ddd">1行1列 span4</div>
  <div class="span5 offset1" style="background:#ddd">1行2列 span5 offset1</div>
</div>
   
## 列嵌套
   
>在span*容器内使用div.row>div.span*
   
   
<div class="row" style="background:#ddd">
  <div class="span9">
    Level 1 column
    <div class="row" style="background:#eee">
      <div class="span6" style="background:#aaa">Level 2</div>
      <div class="span3" style="background:#aaa">Level 2</div>
    </div>
  </div>
</div>
   
   
```html
<div class="row">
  <div class="span9">
    Level 1 column
    <div class="row">
      <div class="span6">Level 2</div>
      <div class="span3">Level 2</div>
    </div>
  </div>
</div>
```
   
##  .container
> 定义一个 宽度为 940px，居中,清除两侧浮动元素，可获取内部内容高度的容器
<div class="container" style="background:#ddd">
    ...
</div>
   
```css
.container {
  margin-right: auto;
  margin-left: auto;*zoom: 1;
}
   
.container:before,.container:after {
  display: table;
  line-height: 0;
  content: "";
}
   
.container:after {
  clear: both;
}
```
   
# Fluid grid system .row-fluid
>用 .row-fluid 代替 .row <br/>
>用 .container-fluid"代替 .container
   
# 工具类
   
<div class="well">
  .well效果
</div>
   
   
<div class="well well-large">
  .well .well-large效果
</div>
   
   
<div class="well well-small">
  .well .well-small
</div>
   
<div class="alert">
    <code>&lt;button class="close"&gt;&times;&lt;/button&gt;</code>效果
    <button class="close">&times;</button>
</div>
   
<code>.pull-right </code> <code>.pull-left</code> <code>.muted</code> <code>.clearfix</code>
```css
.pull-right {
  float: right;
}
.pull-left {
  float: left;
}
.muted {
  color: #999;
}
.clearfix {
  *zoom: 1;
  &:before,
  &:after {
    display: table;
    content: "";
  }
  &:after {
    clear: both;
  }
}
```
