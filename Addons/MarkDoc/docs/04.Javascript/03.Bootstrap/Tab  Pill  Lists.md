Nav : tabs, pills
==========================

|基类|tab|pill|list|stackable(垂直的nav)|li禁用标识位|li激活标识位|li标题标识位|
|----|---------|------|------|------|------|------|------|
|nav|nav-tabs|nav-pills|nav-list|nav-stacked|disabled|active|nav-header|

class="nav nav-tabs"
-------------------

<ul class="nav nav-tabs">
  <li class="active">
    <a href="#">Home</a>
  </li>
  <li><a href="#">About</a></li>
</ul>
```html
<ul class="nav nav-tabs">
  <li class="active">
    <a href="#">Home</a>
  </li>
  <li><a href="#">...</a></li>
</ul>
```
class="nav nav-pills"
--------------------

<ul class="nav nav-pills">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
</ul>

```html
<ul class="nav nav-pills">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
</ul>           
```
   
class="nav nav-list"
--------------

<ul class="nav nav-list">
  <li class="nav-header">List header</li>
  <li><a href="#">Home</a></li>
  <li class="active"><a href="#">Library</a></li>
  <li>
        <ul class="nav nav-list">
            <li class="nav-header">子list</li>
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Library</a></li>
            <li><a href="#">other</a></li>
        </ul>
  </li>
</ul>

```html
<ul class="nav nav-list">
  <li class="nav-header">List header</li>
  <li class="active"><a href="#">Home</a></li>
  <li><a href="#">Library</a></li>
  ...
</ul>
```
class="nav nav-tabs nav-stacked" class="nav nav-pills nav-stacked"
---------------

<div style='height:150px'>
<ul class="nav nav-tabs nav-stacked pull-left">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
</ul>
<ul class="nav nav-pills nav-stacked pull-right">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
</ul>
</div>

```html
<ul class="nav nav-tabs nav-stacked">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
</ul>
```

```html
<ul class="nav nav-pills nav-stacked">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
</ul>
```
   
为nav 添加下拉列表
--------------

   
   
<ul class="nav nav-pills">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Help</a></li>
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu"  style='padding-left:0;margin:0'>
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </li>
</ul>

```html
<ul class="nav nav-pills" >
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Help</a></li>
                 
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dropdown
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li class="divider"></li>
                    ...
                </ul>
              </li>
</ul>
```
   
选项卡
---------
   
|外层容器控制tabs位置{col:3}        |选项盘容器            |选项卡容器        |             选项盘|                 上下结构|
|----------|-----------|------------|----------------------|------------------|-------------------|-------------------------|
|.tabbable(基本类){col:3}           |div.tab-content{row:2}|ul.nav-tabs{row:2}|div.tab-pane{row:2}|外层容器>(tabs容器+选项盘容器)|
|.tabs-left|.tabs-right|.tabs-below |外层容器>(选项盘容器+tabs容器){col:4}|
   
   
<div class="tabbable tabs-below">
     <div class="tab-content">
                <div class="tab-pane" id="A">
                  <p>外层容器使用.tabbable和.tabs-below,选项盘容器在上,tabs容器在下</p>
                </div>
                <div class="tab-pane" id="B">
                  <p>外层容器使用.tabbable和.tabs-left</p>
                </div>
                <div class="tab-pane active" id="C">
                  <p>外层容器使用.tabbable和.tabs-right</p>
                </div>
                <div class="tab-pane" id='D'>
                    <p class="text-warning">选项卡属性:&lt;a href="#targetID" data-toggle="tab"&gt;</p>
                </div>
                 <div class="tab-pane" id='E'>
                    <p class="text-warning">选项盘ID值是选项卡切换的目标标识,.active是激活的选项盘</p>
                </div>
                <div class="tab-pane" id='F'>
                    <p class="text-warning">选项盘添加 <em>.fade</em>,并且为选项卡绑定click事件:
                       
                    </p>
    <pre class="prettyprint">
$('.tabbable a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})</pre>
                      <p>或者直接对选项卡jQuery对象执行:<code>$('').tab('show')</code></p>
                </div>
       </div>
       <ul class="nav nav-tabs">
          <li class=""><a href="#A" data-toggle="tab">tabs在底部</a></li>
          <li class=""><a href="#B" data-toggle="tab">tabs在左侧</a></li>
          <li class="active"><a href="#C" data-toggle="tab">tabs在右侧</a></li>
          <li class=""><a href="#D" data-toggle="tab">选项卡属性</a></li>
          <li class=""><a href="#E" data-toggle="tab">选项盘ID,class</a></li>
          <li class=""><a href="#F" data-toggle="tab">切换特效</a></li>
       </ul>
</div>

```html
<div class="tabbable tabs-below">
              <div class="tab-content">
                <div class="tab-pane" id="A">
                  <p>I'm in Section A.</p>
                </div>
                <div class="tab-pane" id="B">
                  <p>Howdy, I'm in Section B.</p>
                </div>
                <div class="tab-pane active" id="C">
                  <p>What up girl, this is Section C.</p>
                </div>
              </div>
              <ul class="nav nav-tabs">
                <li class=""><a href="#A" data-toggle="tab">Section 1</a></li>
                <li class=""><a href="#B" data-toggle="tab">Section 2</a></li>
                <li class="active"><a href="#C" data-toggle="tab">Section 3</a></li>
              </ul>
</div>
```
   
   
<div class="tabbable tabs-right">
  <ul class="nav nav-tabs">
            <li class=""><a href="#rA" data-toggle="tab">Section 1</a></li>
                <li class=""><a href="#rB" data-toggle="tab">Section 2</a></li>
                <li class="active"><a href="#rC" data-toggle="tab">Section 3</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane" id="rA">
                  <p>I'm in Section A.</p>
                </div>
                <div class="tab-pane" id="rB">
                  <p>Howdy, I'm in Section B.</p>
                </div>
                <div class="tab-pane active" id="rC">
                  <p>What up girl, this is Section C.</p>
                </div>
  </div>
</div>
   
Tab特效
---------

|@|@|
|-----------------|------------|
|**文件**|`bootstrap-tab.js`|
|**Tab a标签属性标记**  |`data-toggle="tab"` `data-toggle="pill"` `href="#targetID"`|
|**method**           |`$('a').tab('show')`                  |
|**event**{row:2}     |show (切换开始之前执行的事件)             |
|shown (切换结束后执行的事件) {col:2}|

```js
$('#myTab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})
```

```js
$('a[data-toggle="tab"]').on('show', function (e) {
  e.target // 即将激活的tab
  e.relatedTarget // 当前激活的tab
})
$('a[data-toggle="tab"]').on('shown', function (e) {
  e.target // 激活的tab
  e.relatedTarget // 上一次激活的tab
})
```
