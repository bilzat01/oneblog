1.8 version
==============

1.8不推荐的
------------

deferred.pipe()

1.8新增的
------

<p class="api">addBack([selector])</p>

将前一个元素选择方法选择的元素merge到上一个方法产生的结果集中返回.

selector参数可用于过滤.

举例:

```
<ul>
   <li>list item 1</li>
   <li>list item 2</li>
   <li class="third-item">list item 3</li>
   <li>list item 4</li>
   <li>list item 5</li>
</ul>

<script>
$('li.third-item').nextAll().addBack().css('background-color', 'red');
</script>
```

上面的js中,$('li.third-item')选择到了第三个li元素,然后nextAll()选择到了第四和第五个li,
addBack()方法将nextAll选择的元素与$('li.third-item')选择的元素合并在一起返回,最后返回的结果
集合含有第三,第四,第五个li

<p class="api">jQuery.parseHTML()</p>

该工具方法使用原生DOM元素创建函数将html代码转换成DOM元素数组(不是jqurey元素)


1.8变化
---------

### $(":eq(-index)")

:eq选择器支持负值索引,表示从结果集末尾开始计算

1.9 verison
=========

1.9变化
-----

### .css(string|Array)

css方法现在支持数组作为参数,此事返回值为json对象

例如:`$(this).css( ["width", "height", "color", "background-color"] )`

返回值类似:`{width: "1240px", height: "54px", color: "rgb(51, 51, 51)", background-color: "rgba(0, 0, 0, 0.180392)"}`

1.9新增
--------

<p class="api">finish([queue])</p>

当调用该方法时,元素上正在运行的动画将立即停止,队列之后的所有动画也不再执行,元素的状态设置为所有动画正常结束后的状态.可以理解为,所有动画0秒内执行完成.

### 新增选择器

:first-of-type 

:lang() 

:last-of-type 

:nth-last-child() 

:nth-last-of-type() 

:nth-of-type() 

:only-of-type 

:root 

:target 

