<?php
return array(
	'docs_path'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' =>'手册路径',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => 'docs',	//表单的默认值
		'tip'	=> '相对于插件根目录'
	),
	'title'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '文档标题:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => 'jay\'s的实验室',	//表单的默认值
	),
	'tagline'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '品牌口号:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',	//表单的默认值
	),
	'image'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '品牌图片:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',				//表单的默认值
		'tip'	=> '图像可以是本地图像或者远程图像.',
	),
	'theme'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '主题:',		//表单的文字
		'type'  => 'select',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'options'=>array(
			'blue'  => 'blue',
			'green' => 'green',
			'navy'  => 'navy',
			'red'   => 'red',
		),
		'value' => 'blue',	//表单的默认值
	),
	'date_modified'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '修改日期显示:',		//表单的文字
		'type'  => 'select',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'options'=>array(
			true  => '是',
			false => '否',
		),
		'value' => true,	//表单的默认值
	),
	'float'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '代码悬浮:',		//表单的文字
		'type'  => 'select',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'options'=>array(
			true  => '是',
			false => '否',
		),
		'value' => 'true',	//表单的默认值
		'tip'=>'默认设置你的文档中的代码块会被悬浮于内容的右侧.(译者注:当屏幕超过1150px的时候) 如果需要关闭该特性,只需要设置 float 属性为 false.',
	),
	'repo'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => 'GitHub 版本控制:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',	//表单的默认值
		'tip' =>'添加一个带有 \'Fork me on GitHub\'内容的横幅彩带的配置项',
	),
	'twitter'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => 'Twitter:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',	//表单的默认值
		'tip'=>'包含twitter关注按钮在侧边栏.'
	),
	'links'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '链接:',		//表单的文字
		'type'  => 'textarea',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',	//表单的默认值
		'tip'	=> '包含用户定制链接在侧边栏配置如下 title:link\\n'
	),
	'colors'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '颜色:',		//表单的文字
		'type'  => 'textarea',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',	//表单的默认值
		'tip'	=> '当主题为自定义时，可以定义的颜色 格式 class:#hex\\n',
	),
	'google_analytics'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => 'Google分析:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'tip'	=> '这里将会嵌入Google分析跟踪码. 如 UA-XXXXXXXXX-XX',
		'value' => '',	//表单的默认值
	),
	'piwik_analytics'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => 'Piwik分析:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => '',	//表单的默认值
		'tip'	=> '这里将会嵌入Piwik跟踪码. 如my-url-for-piwik.com"',
	),
	// 'ignore'=>array(					//配置在表单中的键名 ,这个会是config[random]
	// 	'title' => '过滤忽视文件:',		//表单的文字
	// 	'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
	// 	'value' => '',	//表单的默认值

	// ),
	'timezone'=>array(					//配置在表单中的键名 ,这个会是config[random]
		'title' => '时区:',		//表单的文字
		'type'  => 'text',		 	//表单的类型：text、textarea、checkbox、radio、select等
		'value' => 'Asia/Shanghai',	//表单的默认值
	),

);

