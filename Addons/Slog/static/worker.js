function messageHandler(evt){
	var event_url = '/admin.php?s=/Addons/execute/_addons/Slog/_controller/Slog/_action/debug/ajax/1';
	//声明一个EventSource:source;
	var source;
	if(evt.data){
		try{
				source=new EventSource(event_url);
				source.onopen=function(event){
					// console.log("连接已经建立，状态号："+this.readyState);
				}
				source.onmessage=function(event){
					var data = JSON.parse(event.data)
					postMessage(data);
				}
				source.onerro=function(event){
					postMessage("出错，信息状态号是："+this.readyState);
				}
			}catch(err){
				postMessage('出错啦，错误信息：'+err.message);
			}
	}else{
		postMessage('已经退出！');
		source.close();
		source=null;
	}
}
self.addEventListener('message',messageHandler,false);
