<?php

namespace Addons\BookmarkCheck\Controller;
use Home\Controller\AddonsController;

class BookmarkCheckController extends AddonsController{

	public function checkUrl(){
		$urls = I('urls', '');
		$id = I('id', 0);
		if($id == 0){
			$list = explode(PHP_EOL, $urls);
			$url = $list[$id];
			unset($list[$id]);
		}else{
			$list = session('batch_urls_check');
			$url = $list[$id];
			unset($list[$id]);
		}

	    if(!empty($url)){
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			// curl_setopt($curl, CURLOPT_NOBODY, true);
			curl_setopt($curl, CURLINFO_HEADER_OUT, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT,1);
			curl_setopt($curl, CURLOPT_TIMEOUT, 3);

		    // 发送请求
		    $result = curl_exec($curl);
		    $found = false;
		    // 如果请求没有发送失败
		    if ($result !== false) {
		        // 再检查http响应码是否为200
		        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		        if ($statusCode == 200) {
		            $found = true;
	        	    $pos = strpos($result,'utf-8');
					if($pos === false){
						$result = iconv("gbk","utf-8",$result);
					}
					preg_match("/<title>(.*)<\/title>/i",$result, $title);
					$title = $title[1];
		        }else{
		        	$title = $statusCode;
		        }
		    }
		    curl_close($curl);
	    	$class = $found? 'success' : 'error';
	    	$result_text = $found? '有效' : '无效';
	    	$index = $id+1;
	    	$msg = <<<tr
<tr id="index_{$id}" class="{$class}"><td>{$index}</td><td><a href="{$url}" target="_blank">{$url}</a></td><td width="60">{$title}</td><td width="30">{$result_text}</td></tr>
tr;
			echo "process($id, '{$msg}');";
            session('batch_urls_check', $list);
	    }else{
	    	session('batch_urls_check', null);
            exit("process(-1,'');");
	    }
	}

	public function upload(){
		reset($_FILES);
        /* 返回JSON数据 */
        if($data['file']['error']){
        	echo 0;
        	exit();
        }
        $content = file_get_contents($_FILES['bookmark_upload']['tmp_name']);
        if(!$this->is_utf8($content))
        	$content = iconv("gbk","utf-8",$content);
        exit($content);
	}

	public function is_utf8($str) {
        $c=0; $b=0;
        $bits=0;
        $len=strlen($str);
        for($i=0; $i<$len; $i++){
            $c=ord($str[$i]);
            if($c > 128){
                if(($c >= 254)) return false;
                elseif($c >= 252) $bits=6;
                elseif($c >= 248) $bits=5;
                elseif($c >= 240) $bits=4;
                elseif($c >= 224) $bits=3;
                elseif($c >= 192) $bits=2;
                else return false;
                if(($i+$bits) > $len) return false;
                while($bits > 1){
                    $i++;
                    $b=ord($str[$i]);
                    if($b < 128 || $b > 191) return false;
                    $bits--;
                }
            }
        }
        return true;
    }

	public function rules(){
		$list = include ONETHINK_ADDON_PATH.'BookmarkCheck/config.php';
		$this->ajaxReturn($list['rules']['options']);
	}
}
